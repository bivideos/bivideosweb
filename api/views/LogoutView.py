# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import redirect
from django.views import View
from django.contrib.auth import logout
# Create your views here.

class LogoutView(View):
        
    def get(self, request):
        logout(request)
        return redirect('/')
        
    