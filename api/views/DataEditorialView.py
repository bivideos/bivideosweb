# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import redirect
from django.views import View
from django.contrib.auth import logout
from django.http import JsonResponse
from bivideosweb.utils import get_editorial_table,get_editorial_map

class DataEditorialView(View):
        
    def get(self, request):
        brands = request.GET.get('brands','').upper().split(',') if len(request.GET.get('brands',''))>0 else []
        products = request.GET.get('products','').upper().split(',') if len(request.GET.get('products',''))>0 else []
        publish_date = request.GET.get('publish_date','').split(',') if request.GET.get('publish_date','').count(',')>0 else []
        play_date = request.GET.get('play_date','').split(',') if request.GET.get('play_date','').count(',')>0 else []

        result_table = get_editorial_table(brands,products,publish_date,play_date)
        result_map = get_editorial_map(brands,products,publish_date,play_date)
        return JsonResponse({'success': {
            'table' : result_table,
            'map' : result_map
        }})
        
    