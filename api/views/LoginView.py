# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render
from django.http import JsonResponse
from django.views import View
from django.contrib.auth import authenticate, login
from django.views.decorators.http import require_POST
from django.utils.decorators import method_decorator
from api.decorators import json_parse
import json
# Create your views here.

class LoginView(View):
        
    @method_decorator(json_parse)
    def post(self, request):
        user = authenticate(request, username=request.json['username'], password=request.json['password'])
        if user is not None:
            login(request, user)
            # A backend authenticated the credentials
            return JsonResponse({'success':True})
        else:
            # No backend authenticated the credentials
            return JsonResponse({'success':False})
    