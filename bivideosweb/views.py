from django.http import HttpResponse
from django.template import loader

from django.db import connections
import simplejson as xjson
from utils import *

def index(request):
    
    template = loader.get_template('bivideosweb/index.html')
    context = {
        
    }
    return HttpResponse(template.render(context, request))


def comercial(request):
    
    context = {}
    context['brands'] = get_brands()
    context['min_publish_date'],context['max_publish_date'] = get_min_max_publish_date()
    context['min_play_date'],context['max_play_date'] = get_min_max_play_date()
    
    context['result_table'] = get_comercial_table()

    template = loader.get_template('bivideosweb/comercial.html')
    context = {
        "context" : xjson.dumps(context,use_decimal=True)
    }
    return HttpResponse(template.render(context, request))


def editorial(request):
    
    context = {}
    context['brands'] = get_brands()
    context['min_publish_date'],context['max_publish_date'] = get_min_max_publish_date()
    context['min_play_date'],context['max_play_date'] = get_min_max_play_date()
    
    context['result_table'] = get_editorial_table()
    context['map_data'] = get_editorial_map()

    template = loader.get_template('bivideosweb/editorial.html')
    context = {
        "context" : xjson.dumps(context)
    }
    return HttpResponse(template.render(context, request))