
from django.db import connections
from django.core.cache import cache
import hashlib
import json
cache_prefix = 'v1_'
cache_time = 1#3600

def get_brands():

    cache_key = cache_prefix+'get_brands'
    result = cache.get(cache_key)
    if result : return result

    brands = {}
    with connections['dashboard'].cursor() as cursor:
        cursor.execute("SELECT 	brand,producto FROM video_brand GROUP BY brand,producto")
        for row in cursor.fetchall() :
            brand = row[0].title()
            producto = row[1]
            if not brand in brands :
                brands[brand] = {'brand':brand,'productos':[]}
            if producto and not producto in brands[brand]['productos']:
                brands[brand]['productos'].append(producto)
        cache.set(cache_key, brands, cache_time)
        return brands


def get_min_max_publish_date():

    cache_key = cache_prefix+'get_min_max_publish_date'
    result = cache.get(cache_key)
    if result : return result

    with connections['dashboard'].cursor() as cursor:
        cursor.execute("SELECT min(first_published_at), max(first_published_at) FROM video_brand")
        row = cursor.fetchone()
        min_publish_date = row[0].strftime('%Y-%m-%d')
        max_publish_date = row[1].strftime('%Y-%m-%d')
        result = (min_publish_date, max_publish_date)
        cache.set(cache_key, result, cache_time)
        return result


def get_min_max_play_date():

    cache_key = cache_prefix+'get_min_max_play_date'
    result = cache.get(cache_key)
    if result : return result

    with connections['dashboard'].cursor() as cursor:
        cursor.execute("SELECT min(start_date), max(start_date) FROM ooyala_resume")
        row = cursor.fetchone()
        min_play_date = row[0].strftime('%Y-%m-%d')
        max_play_date = row[1].strftime('%Y-%m-%d')
        result = (min_play_date, max_play_date)
        cache.set(cache_key, result, cache_time)
        return result
    
    

def get_editorial_table(brand=[], product=[], publish_date=[], play_date=[]):
    data_md5 = hashlib.md5(json.dumps([brand,product,publish_date,play_date], sort_keys=True)).hexdigest()
    cache_key = cache_prefix+'get_editorial_table_'+data_md5
    result = cache.get(cache_key)
    if result : return result

    with connections['dashboard'].cursor() as cursor:
        cursor.execute("""
            SELECT
                video_brand.brand as brand,
                SUM(ooyala_resume.uniq_plays_requested) AS usuarios_unicos,
                SUM(ooyala_resume.plays_requested) AS cant_reprod,
                SUM(ooyala_resume.plays_requested_movil) AS cant_reprod_mobi,
                SUM(ooyala_resume.plays_requested_desktop) AS cant_reprod_desk,
                ROUND(100*(CASE WHEN SUM(ooyala_resume.video_starts)>0 THEN (((SUM(ooyala_resume.video_starts) - SUM(ooyala_resume.playthrough_25))*0.125)+((SUM(ooyala_resume.playthrough_25)	- SUM(ooyala_resume.playthrough_50))*0.375)+((SUM(ooyala_resume.playthrough_50)	- SUM(ooyala_resume.playthrough_75))*0.625)+((SUM(ooyala_resume.playthrough_75)	- SUM(ooyala_resume.playthrough_100))*0.875)+SUM(ooyala_resume.playthrough_100))/SUM(ooyala_resume.video_starts) ELSE 0 END),2) || '%%' as porc_repr,
                SUM(ooyala_resume.displays) AS cant_exhib,
                SUM(ooyala_resume.displays_movil) AS cant_exhib_mobi,
                SUM(ooyala_resume.displays_desktop) AS cant_exhib_desk,
                SUM(ooyala_resume.replays) AS replays,
                COUNT(DISTINCT ooyala_resume.asset) AS cant_vid
            FROM video_brand
            INNER JOIN ooyala_resume ON video_brand.media_id = ooyala_resume.asset
            WHERE 1=1
                AND (NOT %s OR video_brand.brand in %s)
                AND (NOT %s OR video_brand.producto in %s)
                AND (NOT %s OR (video_brand.first_published_at >= %s::date AND video_brand.first_published_at <= %s::date))
                AND (NOT %s OR (ooyala_resume.start_date >= %s::date AND ooyala_resume.start_date <= %s::date))
            GROUP BY
                video_brand.brand
        """, [
            len(brand)>0, tuple(brand) if brand else tuple([None]),
            len(product)>0, tuple(product) if product else tuple([None]),
            len(publish_date)>1, publish_date[0] if len(publish_date)>0 else '1-01-01', publish_date[1] if len(publish_date)>1 else '1-01-01',
            len(play_date)>1, play_date[0] if len(play_date)>0 else '1-01-01', play_date[1] if len(play_date)>0 else '1-01-01',
        ])
        result = []
        for row in cursor.fetchall() :
            # r = [
            #     row[0],
            #     "{:,}".format(row[1]),
            #     "{:,}".format(row[2]),
            #     "{:,}".format(row[3]),
            #     "{:,}".format(row[4]),
            #     "{:,}".format(row[5]),
            #     "{:,}".format(row[6]),
            #     "{:,}".format(row[7]),
            #     "{:,}".format(row[8]),
            #     "{:,}".format(row[9]),
            # ]
            result.append(row)
        cache.set(cache_key, result, cache_time)
        return result


def get_editorial_map(brand=[], product=[], publish_date=[], play_date=[]):
    data_md5 = hashlib.md5(json.dumps([brand,product,publish_date,play_date], sort_keys=True)).hexdigest()
    cache_key = cache_prefix+'get_editorial_map_'+data_md5
    result = cache.get(cache_key)
    if result : return result

    with connections['dashboard'].cursor() as cursor:
        cursor.execute("""
            SELECT
                ooyala_country.country as country,
                SUM(ooyala_country.plays_requested) AS plays_requested
            FROM
                video_brand
            INNER JOIN ooyala_country ON video_brand.media_id = ooyala_country.asset
            WHERE 1=1
                AND (NOT %s OR video_brand.brand in %s)
                AND (NOT %s OR video_brand.producto in %s)
                AND (NOT %s OR (video_brand.first_published_at >= %s::date AND video_brand.first_published_at <= %s::date))
                AND (NOT %s OR (ooyala_country.start_date >= %s::date AND ooyala_country.start_date <= %s::date))
            group by
                ooyala_country.country
        """, [
            len(brand)>0, tuple(brand) if brand else tuple([None]),
            len(product)>0, tuple(product) if product else tuple([None]),
            len(publish_date)>1, publish_date[0] if len(publish_date)>0 else '1-01-01', publish_date[1] if len(publish_date)>1 else '1-01-01',
            len(play_date)>1, play_date[0] if len(play_date)>0 else '1-01-01', play_date[1] if len(play_date)>0 else '1-01-01',
        ])
        result = []
        for row in cursor.fetchall() :
            result.append(row)
        cache.set(cache_key, result, cache_time)
        return result


def get_comercial_table(brand=[], publish_date=[], play_date=[]):
    data_md5 = hashlib.md5(json.dumps([brand,publish_date,play_date], sort_keys=True)).hexdigest()
    cache_key = cache_prefix+'get_comercial_table_'+data_md5
    result = cache.get(cache_key)
    if result : return result

    with connections['dashboard'].cursor() as cursor:
        cursor.execute("""
            SELECT 
                a.brand,
                a.notas_generadas,
                a.notas_video,
                CASE WHEN a.notas_generadas>0 THEN round(100*a.notas_video::decimal/a.notas_generadas) ELSE 0 END as ratio_notas_video,
                b.videos_subidos,
                b.videos_monet,
                b.videos_no_monet,
                CASE WHEN b.videos_subidos > 0 THEN round(100*b.videos_monet::decimal/b.videos_subidos) ELSE 0 END as ratio_videos_monet,
                CASE WHEN b.videos_subidos > 0 THEN round(100*b.videos_no_monet::decimal/b.videos_subidos) ELSE 0 END as ratio_videos_no_monet,
                b.cant_reprod_tot,
                b.cant_reprod_monet,
                c.impresiones_codigo as cant_reprod_monet_dfp,
                b.cant_reprod_no_monet,
                CASE WHEN b.cant_reprod_tot > 0 THEN round(100*b.cant_reprod_monet::decimal/b.cant_reprod_tot) ELSE 0 END as rat_reprod_monet,
                CASE WHEN b.cant_reprod_tot > 0 THEN round(100*b.cant_reprod_no_monet::decimal/b.cant_reprod_tot) ELSE 0 END as rat_reprod_no_monet
            FROM
            (
                SELECT
                    xalok_resume.brand,
                    SUM(xalok_resume.notas_generadas) as notas_generadas,
                    SUM(xalok_resume.notas_video) as notas_video
                FROM
                    xalok_resume
                WHERE 1=1
                    AND (NOT %s OR xalok_resume.brand in %s)
                    AND (NOT %s OR (xalok_resume.first_published_at >= %s::date AND xalok_resume.first_published_at <= %s::date))
                GROUP BY xalok_resume.brand
            ) a
            LEFT JOIN (
                SELECT
                    video_brand.brand,
                    COUNT(DISTINCT video_brand.media_id ) as videos_subidos,
                    COUNT(DISTINCT (CASE WHEN video_brand.video_publicidad>0 THEN video_brand.media_id END )) AS videos_monet,
				    COUNT(DISTINCT (CASE WHEN video_brand.video_publicidad>0 THEN NULL ELSE video_brand.media_id END )) AS videos_no_monet,
                    SUM(ooyala_resume.plays_requested ) AS cant_reprod_tot,
                    SUM(CASE WHEN video_brand.video_publicidad > 0 THEN ooyala_resume.plays_requested ELSE 0 END ) AS cant_reprod_monet,
                    SUM(CASE WHEN video_brand.video_publicidad > 0 THEN 0 ELSE ooyala_resume.plays_requested END ) AS cant_reprod_no_monet
                FROM
                    video_brand
                INNER JOIN ooyala_resume ON ooyala_resume.asset = video_brand.media_id
                WHERE 1 = 1
                    AND (NOT %s OR video_brand.brand in %s)
                    AND (NOT %s OR (video_brand.first_published_at >= %s::date AND video_brand.first_published_at <= %s::date))
                    AND (NOT %s OR (ooyala_resume.start_date >= %s::date AND ooyala_resume.start_date <= %s::date))
                GROUP BY video_brand.brand
            ) b ON a.brand = b.brand
            LEFT JOIN (
                SELECT
                    upper(dfp.brand) as xbrand,
                    SUM(dfp.total_code) as impresiones_codigo
                FROM
                    dfp
                WHERE 1 = 1
                    AND (NOT %s OR upper(dfp.brand) in %s)
                    AND (NOT %s OR (dfp.start_date >= %s::date AND dfp.start_date <= %s::date))
                GROUP BY xbrand
            ) c ON c.xbrand = a.brand
        """, [
            len(brand)>0, tuple(brand) if brand else tuple([None]),
            len(publish_date)>1, publish_date[0] if len(publish_date)>0 else '1-01-01', publish_date[1] if len(publish_date)>1 else '1-01-01',
            len(brand)>0, tuple(brand) if brand else tuple([None]),
            len(publish_date)>1, publish_date[0] if len(publish_date)>0 else '1-01-01', publish_date[1] if len(publish_date)>1 else '1-01-01',
            len(play_date)>1, play_date[0] if len(play_date)>0 else '1-01-01', play_date[1] if len(play_date)>0 else '1-01-01',
            len(brand)>0, tuple(brand) if brand else tuple([None]),
            len(play_date)>1, play_date[0] if len(play_date)>0 else '1-01-01', play_date[1] if len(play_date)>0 else '1-01-01', 
        ])
        result = []
        for row in cursor.fetchall() :
            # r = [
            #     row[0],
            #     "{:,}".format(row[1]),
            #     "{:,}".format(row[2]),
            #     "{:,}".format(row[3]),
            #     "{:,}".format(row[4]),
            #     "{:,}".format(row[5]),
            #     "{:,}".format(row[6]),
            #     "{:,}".format(row[7]),
            #     "{:,}".format(row[8]),
            #     "{:,}".format(row[9]),
            # ]
            result.append(row)
        cache.set(cache_key, result, cache_time)
        return result

def get_comercial_dfp_table(brand=[], product=[], publish_date=[], play_date=[]):
    pass