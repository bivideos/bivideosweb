"""bivideosweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required

from bivideosweb import views
from django.contrib.auth import views as auth_views
from api.views.LoginView import LoginView
from api.views.LogoutView import LogoutView
from api.views.DataComercialView import DataComercialView
from api.views.DataEditorialView import DataEditorialView

urlpatterns = [
    url(r'^$', login_required(views.index), name='index'),
    url(r'^login$', auth_views.LoginView.as_view(redirect_authenticated_user=True), name='login'),
    url(r'^api/login$', LoginView.as_view()),
    url(r'^api/logout$', LogoutView.as_view()),

    url(r'^dashboard-comercial$', login_required(views.comercial), name='comercial'),
    url(r'^dashboard-editorial$', login_required(views.editorial), name='editorial'),

    url(r'^api/data-comercial$', login_required(DataComercialView.as_view()), name='api-comercial'),
    url(r'^api/data-editorial$', login_required(DataEditorialView.as_view()), name='api-editorial'),

    url(r'^admin/', admin.site.urls),
]
